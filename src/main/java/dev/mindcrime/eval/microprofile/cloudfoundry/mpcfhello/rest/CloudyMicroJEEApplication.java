package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

// configure base path for provided REST services
@ApplicationPath("/services")
public class CloudyMicroJEEApplication extends Application
{

}
