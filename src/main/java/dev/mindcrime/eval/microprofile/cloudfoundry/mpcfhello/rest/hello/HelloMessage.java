package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.hello;


import lombok.*;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class HelloMessage {
    private LocalDateTime timestamp;
    private String message;
}
