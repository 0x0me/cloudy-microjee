package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.healthservice;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@Health
public class HealthCheckService implements HealthCheck {

    @Override
    public HealthCheckResponse call() {

        return HealthCheckResponse.builder()
                .name("cloudy-microjee")
                .withData("free-memory", Runtime.getRuntime().freeMemory())
                .withData("max-memory", Runtime.getRuntime().maxMemory())
                .withData("total-memory", Runtime.getRuntime().totalMemory())
                .up()
                .build();
    }
}
