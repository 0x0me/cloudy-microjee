package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.hello;

import dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.timeservice.LocalTimeService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@ApplicationScoped
@Path("/hello")
public class HelloWorldEndpoint {

	@Inject
	@ConfigProperty(name = "greeting.message")
	private String greeting;

	@EJB
	private LocalTimeService localTimeService;

	@GET
	@Produces("application/json")
	public Response doGet() {
		HelloMessage msg  = HelloMessage.builder()
				.message("Thorntail says:"+ greeting)
				.timestamp(localTimeService.getCurrentTime())
				.build();

		return Response.ok(msg)
				.build();
	}
}
