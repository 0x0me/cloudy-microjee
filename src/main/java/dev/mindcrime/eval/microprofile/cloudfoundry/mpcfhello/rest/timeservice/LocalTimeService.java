package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.timeservice;

import javax.ejb.Stateless;
import java.time.LocalDateTime;

@Stateless
public class LocalTimeService {

    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }
}
