package dev.mindcrime.eval.microprofile.cloudfoundry.mpcfhello.rest.timeservice;


import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
@Path("/current-time")
public class TimeServiceEndpoint {

	@EJB
	private LocalTimeService localTimeService;

	@GET
	@Produces("application/json")
	public Response getCurrentTime() {

		String timestamp = localTimeService.getCurrentTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

		Map<String,String> data = new HashMap<>();
		data.put("localtime", timestamp);
		return Response.ok(data)
				.build();
	}
}
